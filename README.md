# Working with JSON in python
- Module Use: json, ...
- Date: 04 Sep. 2020
- Time: 12:43 AM
- RemoteURL: [Gitlab URL](https://gitlab.com/pbqre/workingwithjson)
----

## Introduction
- *Some Basic Terminology, to begin with...*
- **Serialization**: The process of encoding JSON data.
- **Deserialization**: Opp. of Serialization.

## JSON module
- `json.dump(data, file_object)` is used to write into json file.
- `json.dumps(data, indent=4)` will serialize json data into string.
- `json.loads(data)` corresponding deserialize the data.
- `json.load(filepointer)` to deserialize the file data