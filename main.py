#!/usr/bin/env python

import json
import pprint

__file_name = "./database.json"
data = {
    "president": {
        "name": "NArendra Modi",
        "Age": "56"
    }
}

# Serialization
with open(__file_name, "w") as file:
    json.dump(data, file)

# DeSerialization
with open(__file_name, 'r') as file:
    json_data = json.load(file)

print(type(json_data), json_data)
print(json.dumps(data, indent=4))
